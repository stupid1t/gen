package ${packageName}.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ${packageName}.dao.${ClassName}Dao;
import ${packageName}.common.constant.TkClassHolder;
import ${packageName}.framework.condition.tkmybatis.TK;
import ${packageName}.domain.entity.${ClassName};
import ${packageName}.domain.vo.${ClassName}ListPcVO;
import ${packageName}.domain.param.${ClassName}QueryParam;
import ${packageName}.domain.param.${ClassName}UpdateParam;
import ${packageName}.service.I${ClassName}SV;
#set($ignoreColumn = "")
#foreach ($column in $columns)
    #if($column.isInsert=="1" && $column.isEdit!="1" && $pkColumn.columnName != $column.columnName)
        #set($ignoreColumn=$ignoreColumn+'"'+$column.javaField+'",')
    #end
#end
#if($ignoreColumn!="")
#set($ignoreColumn=$ignoreColumn.substring(0,$ignoreColumn.lastIndexOf(',')))
#end
#set($ignoreColumnInsert = "")
#foreach ($column in $columns)
    #if($column.isInsert!="1" && $column.isEdit=="1" && $pkColumn.columnName != $column.columnName)
        #set($ignoreColumnInsert=$ignoreColumnInsert+'"'+$column.javaField+'",')
    #end
#end
#if($ignoreColumnInsert!="")
    #set($ignoreColumnInsert=$ignoreColumnInsert.substring(0,$ignoreColumnInsert.lastIndexOf(',')))
#end
#if($pkColumn.javaField.substring(1,2).matches("[A-Z]"))
    #set($AttrName=$pkColumn.javaField)
#else
    #set($AttrName=$pkColumn.javaField.substring(0,1).toUpperCase() + ${pkColumn.javaField.substring(1)})
#end

/**
* ${functionName}Service业务层处理
*
* @author ${author}
* @date ${datetime}
*/
@Service
@Transactional(rollbackFor = Exception.class)
public class ${ClassName}SVImpl extends TkClassHolder<${ClassName}> implements I${ClassName}SV {

    @Autowired
    private ${ClassName}Dao ${className}Dao;

    /**
     * 查询${functionName}
     *
     * @param ${pkColumn.javaField} ${functionName}ID
     * @return ${functionName}
     */
    @Override
    public ${ClassName} selectById(${pkColumn.javaType} ${pkColumn.javaField}) {
        ${ClassName} ${className} = ${className}Dao.selectByPrimaryKey(${pkColumn.javaField});
        return ${className};
    }

    /**
     * 查询${functionName}列表
     *
     * @param queryParam ${functionName} 查询参数
     * @return ${functionName}
     */
    @Override
    public List<${ClassName}ListPcVO> selectListPC(${ClassName}QueryParam queryParam) {
        List<${ClassName}ListPcVO> ${className}s = ${className}Dao.selectListPC(queryParam);
        return ${className}s;
    }

    /**
     * 查询${functionName}列表
     *
     * @param queryParam ${functionName} 查询参数
     * @return ${functionName}
     */
    @Override
    public List<${ClassName}> selectList(TK<${ClassName}> queryParam) {
        List<${ClassName}> ${className}s = ${className}Dao.selectByExample(queryParam);
        return ${className}s;
    }

    /**
     * 查询${functionName}
     *
     * @param queryParam ${functionName} 查询参数
     * @return ${functionName}
     */
    @Override
    public ${ClassName} selectOne(TK<${ClassName}> queryParam) {
        ${ClassName} ${className} = ${className}Dao.selectOneByExample(queryParam);
        return ${className};
    }

    /**
     * 总计${functionName}
     *
     * @param queryParam  ${functionName}查询参数
     * @return ${functionName}数量
     */
    @Override
    public int countByParams(TK<${ClassName}> queryParam) {
        return ${className}Dao.selectCountByExample(queryParam);
    }

    /**
     * 新增${functionName}
     *
     * @param updateParam ${functionName} 新增参数
     * @return 结果
     */
    @Override
    public ${pkColumn.javaType} insert(${ClassName}UpdateParam updateParam) {
        ${ClassName} entity = updateParam.covert($ignoreColumnInsert);
        entity.set$AttrName(null);
        ${className}Dao.insertSelective(entity);
        return entity.get$AttrName();
    }

    /**
     * 修改${functionName}
     *
     * @param updateParam ${functionName} 修改参数
     * @return 结果
     */
    @Override
    public int updateById(${ClassName}UpdateParam updateParam) {
        ${ClassName} entity = updateParam.covert($ignoreColumn);
        int result = ${className}Dao.updateByPrimaryKeySelective(entity);
        return result;
    }

    /**
     * 修改${functionName}
     *
     * @param updateParam ${functionName} 修改参数
     * @param queryParam ${functionName} 查询参数
     * @return 结果
     */
    @Override
    public int updateByParams(${ClassName}UpdateParam updateParam, TK<${ClassName}> queryParam) {
        ${ClassName} entity = updateParam.covert($ignoreColumn);
        int result = ${className}Dao.updateByExampleSelective(entity, queryParam);
        return result;
    }

    /**
     * 删除${functionName}信息
     *
     * @param ${pkColumn.javaField} ${functionName}ID
     * @return 结果
     */
    @Override
    public int deleteById(${pkColumn.javaType} ${pkColumn.javaField}) {
#if($isLogicDelete)
        return ${className}Dao.updateByPrimaryKeySelective(${ClassName}.builder().${logicDeleteColumn.columnName}(FlagEnum.IS.getKey()).${pkColumn.javaField}(${pkColumn.javaField}).build());
#else
        return ${className}Dao.deleteByPrimaryKey(${pkColumn.javaField});
#end
    }

    /**
     * 删除${functionName}
     *
     * @param queryParam ${functionName} 查询参数
     * @return 结果
     */
    @Override
    public int deleteByParams(TK<${ClassName}> queryParam) {
#if($isLogicDelete)
        return ${className}Dao.updateByExampleSelective(${ClassName}.builder().${logicDeleteColumn.columnName}(FlagEnum.IS.getKey()).build(), queryParam);
#else
        return ${className}Dao.deleteByExample(queryParam);
#end
    }

    /**
     * 批量删除${functionName}
     *
     * @param ${pkColumn.javaField}s 需要删除的${functionName}ID
     * @return 结果
     */
    @Override
    public int deleteByIds(${pkColumn.javaType}[] ${pkColumn.javaField}s) {
#set($AttrName=$pkColumn.columnName.substring(0,1).toUpperCase() + ${pkColumn.columnName.substring(1)})
#if($isLogicDelete)
        return ${className}Dao.updateByExampleSelective(${ClassName}.builder().${logicDeleteColumn.columnName}(FlagEnum.IS.getKey()).build(), SelectWhere().andIn(${ClassName}::get${AttrName}, ${pkColumn.javaField}s).end());
#else
        return ${className}Dao.deleteByExample(SelectWhere().andIn(${ClassName}::get${AttrName},${pkColumn.javaField}s).end());
#end
    }

}
