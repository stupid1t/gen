package ${packageName}.web.controller.${moduleName};

import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.ym.common.utils.page.PageDomain;
import ${packageName}.domain.entity.${ClassName}Entity;
import ${packageName}.domain.vo.${ClassName}ListPcVO;
import ${packageName}.domain.param.${ClassName}QueryParam;
import ${packageName}.domain.param.${ClassName}UpdateParam;
import ${packageName}.service.I${ClassName}SV;
import com.ym.core.annotation.Log;
import com.ym.core.domain.APIResponse;
import com.ym.core.domain.BaseController;
import static com.ym.common.constant.ValidRule.Add;
import static com.ym.common.constant.ValidRule.Update;

import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.Valid;

/**
 * ${functionName}Controller
 * 
 * @author ${author}
 * @date ${datetime}
 */
@Api(tags = "${moduleName}-${functionName}")
@RestController
@Validated
@RequestMapping("/${moduleName}/${businessName}")
public class ${ClassName}Controller extends BaseController {

    @Autowired
    private I${ClassName}SV ${className}SV;

    @ApiOperation("查询${functionName}列表")
    @Log(dataBase = false, printBody = false, printQuery = true)
    @GetMapping("/list")
    public APIResponse<PageInfo<${ClassName}ListPcVO>> list(@Valid ${ClassName}QueryParam queryParam) {
        PageDomain.startPage();
        List<${ClassName}ListPcVO> list = ${className}SV.selectListPC(queryParam);
        return successPage(list);
    }

    @ApiOperation("获取${functionName}详细信息")
    @Log(dataBase = false, printBody = false, printQuery = true)
    @GetMapping(value = "/info")
    public APIResponse<${ClassName}Entity> info(@RequestParam ${pkColumn.javaType} ${pkColumn.javaField}) {
        ${ClassName}Entity data = ${className}SV.selectById(${pkColumn.javaField});
        return successData(data);
    }

    @ApiOperation("新增${functionName}")
    @Log(printQuery = false)
    @PostMapping("/insert")
    public APIResponse insert(@RequestBody @Validated(value = Add.class) ${ClassName}UpdateParam updateParam) {
        ${pkColumn.javaType} ${pkColumn.javaField} = ${className}SV.insertPC(updateParam);
        return successData(${pkColumn.javaField});
    }

    @ApiOperation("修改${functionName}")
    @Log(printQuery = false)
    @PostMapping("/update")
    public APIResponse update(@RequestBody @Validated(value = Update.class) ${ClassName}UpdateParam updateParam) {
        int result = ${className}SV.updateByIdPC(updateParam);
        return successData(result);
    }

    @ApiOperation("删除${functionName}")
    @Log(printQuery = false)
    @PostMapping("/delete")
    public APIResponse delete(@RequestBody @Valid @NotNull ${pkColumn.javaType}[] ${pkColumn.javaField}s) {
        int result = ${className}SV.deleteByIds(${pkColumn.javaField}s);
        return successData(result);
    }
}
